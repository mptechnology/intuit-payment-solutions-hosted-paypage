#Intuit Payment Solutions 
##Hosted PayPage Class

This class with help you integrate Hosted PayPage's into your Web Site 

##Usage

Usage Documentation moved to [Wiki](http://code.mptechnologygroup.com/intuit-payment-solutions-hosted-paypage/wiki/Home)

##Support
This is not a Complete Solution,  if you need commercial support or assistance in developing a solution for your use case contact me for a Quote.

Bugs or erorrs should be reported using the Issue Tracking here on BitBucket,  no other reports will be responded to. 
    


##Lic.

Released under the MIT Lic. 

##Author

MP Technology Group <code@mptechnologygroup.com>  
  
**If you found this Helpful consider a Donation,  your donation helps to fund this and other Open Source Projects**  

[![Donate](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif "Donate")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=TGFGQ976PPRU6)
