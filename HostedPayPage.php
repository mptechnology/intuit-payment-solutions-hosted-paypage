<?php
/**
 * Intuit Payment Solutions
 * Hosted PayPage Class
 *
 * For Usage and Support go to 
 * <http://code.mptechnologygroup.com/intuit-payment-solutions-hosted-paypage/overview>
 * 
 * @author Michael Pope <code@mptechnologygroup.com>
 * 
 * Release under the The MIT License <http://opensource.org/licenses/MIT>
 */

class HostedPayPage{
  
     private $ipp_app_login;
     private $ipp_app_id;
     private $ipp_live;
     private $ipp_conn_ticket;
     private $optional_parms;
  
     function __construct($ipp_conn_ticket,$ipp_app_login,$ipp_app_id,$ipp_live = false) {
         
         $this->ipp_app_id      = $ipp_app_id;
         $this->ipp_app_login   = $ipp_app_login;
         $this->ipp_live        = $ipp_live;
         $this->ipp_conn_ticket = $ipp_conn_ticket;
         
         $this->optional_parms = array(
             'CustomerName'         => false,
             'CustomerStreet'       => false,
             'CustomerCity'         => false,
             'CustomerState'        => false,
             'CustomerPostalCode'   => false,
             'CustomerId'           => false,
             'InvoiceId'            => false,
             'UserData'             => false,
             'ReturnURL'            => false,
             'CancelURL'            => false,
             'ShippingCustomerEmail'=> false,
             'ShippingCustomerPhone'=> false,
             'ShippingNoteToSeller' => false,
             'ShipToName'           => false,
             'ShipToStreet'         => false,
             'ShipToCity'           => false,
             'ShipToState'          => false,
             'ShipToPostalCode'     => false,
             'Comment'              => false
             
         );
         
     }
      
    /**
     * Connects to Inuit and creates the form,  returns the URL for the Hosted PayPage
     * 
     * $ipp_ticket is the Connection Ticket from Step 1 of the Instructions
     * $amount is the dollar amount you want to capture from the customer
     * $live is to set if this should be processed with the PTC testing system or the actual live system
     * 
     * @return array
     */
    public function get_ticket($amount){
       
       
        $url_app_login = urlencode(trim($this->ipp_app_login));
        
        $url_ticket = urlencode(trim($this->ipp_conn_ticket));// URL Encodes the Connection Ticket
        
        $amount = number_format($amount, 2,'.', ''); // Formats the Amount to what IPP is expecting
        
        
        
        $url_base = $this->ipp_live ? "https://paymentservices.intuit.com/" : "https://paymentservices.ptcfe.intuit.com/";
        
        $url  = $url_base;
        $url .= "paypage/ticket/create?AuthModel=desktop&AppLogin=".$url_app_login;
        $url .= "&AuthTicket=".$url_ticket;
        $url .= "&TxnType=Sale&Amount=".$amount;
        $url .= "&IsCustomerFacing=1";
        
        
        
        foreach ($this->optional_parms as $parm => $value){
            if($value){
                $url .= "&".$parm."=".urlencode($value);
            }
        }
        
        
        $conn = curl_init($url);

        curl_setopt($conn, CURLOPT_RETURNTRANSFER, 1);
        
        $response = curl_exec($conn);
        
        if($response === false){
            print "cURL Error Occured.";
            die(curl_error($conn));
        }
        
        curl_close($conn);      
        
        
              
        $response = HostedPayPage::parse_response($response);
        
        if(isset($response['StatusCode']) && $response['StatusCode'] === "0"){
            
            
            $response['url_ticket'] = urlencode(trim($response['Ticket']));
            $response['url_opid']   = urlencode(trim($response['OpId']));
            $response['url'] = $url_base."checkout/terminal?Ticket=".$response['url_ticket']."&OpId=".$response['url_opid']."&action=checkout";
            
                        
            return $response;

        } else{
           
            return $response['StatusMessage'];
                       
        }

    }
    
    
    
    /**
     * 
     * @param type $key
     * @param type $value
     */
    function set_optional_param($key,$value){
        
        if(isset($this->optional_parms[$key])){
            $this->optional_parms[$key] = $value;
            
            return true;
        }
        
        return false;
       
    }
    
    /**
     * Take the Response from Intuit and creates a PHP Array
     * 
     * @param string $response
     * @return array
     */
    
    private function parse_response($response){
        $arr_response = explode("\n",trim($response));
        
        $response = array();
        
        foreach($arr_response as $item ){
            $item = explode("=",$item);
            $response[trim($item[0])] = trim($item[1]);
        }
        return $response;
        
    }
  
}
