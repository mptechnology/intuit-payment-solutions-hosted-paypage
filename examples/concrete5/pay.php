<?php
/**
 * Intuit Payment Solutions
 * Hosted PayPage Class
 * 
 * Concrete5 Single Page Example 
 *
 * For Usage and Support go to 
 * <http://code.mptechnologygroup.com/intuit-payment-solutions-hosted-paypage/overview>
 * 
 * @author Michael Pope <code@mptechnologygroup.com>
 * 
 * Release under the The MIT License <http://opensource.org/licenses/MIT>
 */

require 'HostedPayPage.php';

// Configuration, These Values come from Intuit- See README
$connect_ticket = ''; 
$ipp_app_login  = '';
$ipp_app_id     = '';
$ipp_live       = false;


if(isset($_REQUEST['invoice']) && isset($_REQUEST['amount'])){

    $invoice = (int)$_REQUEST['invoice'];
    $amount = number_format($_REQUEST['amount'], 2,'.', '');
    
    $paypage = new HostedPayPage($connect_ticket, $ipp_app_login, $ipp_app_id, $ipp_live);
    
    $paypage->set_optional_param('ShippingNoteToSeller',"Payment for invoice - $invoice");
        
    $ticket = $paypage->get_ticket($amount);
    
    if(is_array($ticket)  && isset($ticket['url'])){
        
        //If you are redirecting the customer back to your site 
        //Record into a dabase or other Verification system $ticket['OpId'] 
        //This will be used in the payment complete script to verify and match order details
        
        header('Location: '.$ticket['url']);
        
        
    }else{
        
        //Display Error Page
        echo "Sorry there was an error <br />";
        echo "Error was:  $ticket <br/>";
        echo "Please Contact Us";
    }
    
}else{
    
    $form = Loader::helper('form');
    print '<form method="post" action="'.$PHP_SELF.'">';
    print "<div><p><strong>Enter Invoice Number</strong>: ";
    print $form->text('invoice', "", array('style' => 'width: 200px', 'tabindex' => 1)); 
    print "</p></div>";
    print "<div><p><strong>Enter Amount to Pay</strong>: ";
    print $form->text("amount", "0",array('style' => 'width: 200px', 'tabindex' => 2) );
    print "</p></div>";
    print "<div><p>";
    print $form->submit("submit", "Pay Online",array('style' => 'width: 200px', 'tabindex' => 2) );
    print "</p></div></form>";
    
}


