<?php 

/**
 * 
 * This example will show how to Process a Response from the PayPage
 * 
  * 
 * This is a Basic Example, and should not be used as the Final Version, 
 * I have not done must data validation, or error checking in this example,  these things must be done based on your
 * Use case and are outside the scope of this example
 * 
 * If you would like assistance deploying a live system feel free to contact me at sales@cycloness.com
 * 
 * I have used just the Bare Minimum of data that is sent back,  below is the list of Data, and an example of the data returned
 *
 * It is accessable in the $_GET super global,  data in $_GET should be treated as INSECURE and be passed through data validation 
 * when used in a production enviroment 
 * 
 * 
 * OpId=77-977-977-9S3110912124801vvv73vv73vv70d
 * Status=Success
 * OpType=Payment
 * TxnType=Sale
 * MerchantNumber=4506010077672623
 * Order={}
 * StatusCode=100
 * ShipToState=CA
 * ExpirationMonth=6
 * AVSStreetMatch=Pass
 * AVSZipMatch=Pass
 * MaskedCCN=...1111
 * ClientTxnId=q0103c99
 * ShipToPostalCode=12345
 * ExpirationYear=2021
 * CSCMatch=Pass
 * Amount=10.00
 * ShipToStreet=123 Main Street
 * PaymentGroupingCode=5
 * ShipToName=Bob
 * CustomerState=MA
 * CustomerPostalCode=01234
 * CustomerName=Bob
 * CustomerStreet=123 Main Street
 * PaymentType=Credit
 * AuthCode=640333
 * CustomerCity=Boston 
 * TxnTimestamp=2011-09-12T12:51:03-0700
 * ReconBatchId=420110912 1Q12514506010077672623AUTO04
 * TxnId=YY1000598271
 * 
 * 
 * Status Codes are 
 * 
 * Status Code    StatusMessage
    0               OK
    1               Your merchant account is not yet set up to process PayPage transactions.  Please correct before retrying.
    2               There is a problem with your PayPage settings.  Either you have not created a connection ticket or you need to specify how you want to display transaction results.  Refer to your software provider's instructions to correct the problem and retry.  Please contact your software provider if you need further assistance.
    3               Your request failed a validation check: @field is incorrect or missing.  Please contact your software provider before retrying.
    4               Due to a communication failure, the PayPage has lost its link to this transaction.  You must void this transaction and retry. 1.  Call 800-558-9558 with the payment information for this transaction.  2.  If the transaction has gone through, have the support representation void it. 3.  Process the transaction again. Do not retry the transaction without first calling to void.  Failure to void may cause multiple charges on your customer's credit card.
    5               Transaction was not initiated due to a temporary communication failure.  Please retry later.
    6               Your transaction cannot be processed because the request contains an invalid or empty authentication ticket.  Please try again or contact your software provider for help with this issue.
    7               There is a problem with the payment. @field Please ask your customer for another payment method.
    8               Cannot complete transaction electronically. To use this card call the issuer for a voice authorization code or ask customer for another form of payment.
    9               @field Please correct and retry. You can change your security settings to accept transactions that fail security checks.
    10              The credit card presented has failed one or more of your security settings: @field failed.
    11              Request failed.  Usually this happens when funds were already captured, or the original authorization expired.  Occasionally the cause may be a temporary network failure.
    100             Transaction succeeded but Shipping Address does not match Billing Address.
 */


$status     = $_GET['StatusCode'];
$OpId       = $_GET['OpId'];
$trans_id   = $_GET['TxnId'];

if($status == 0){
    
    include 'html/success.php';
    
}else{
    include 'html/error.php';
}