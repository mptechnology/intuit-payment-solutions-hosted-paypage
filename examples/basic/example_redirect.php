<?php
/**
 * 
 * This example will show how to Generate a Payment Page, and then Redirect the customer to Inuit to pay
 * 
 * This example we will assume you have setup a Simple "Buy Now" Link lile in "example_buynow.php"
 * 
 * This is a Basic Example, and should not be used as the Final Version, 
 * I have not done must data validation, or error checking in this example,  these things must be done based on your
 * Use case and are outside the scope of this example
 * 
 * If you would like assistance deploying a live system feel free to contact me at sales@cycloness.com
 * 
 */

require 'HostedPayPage.php';

// Configuration, These Values come from Intuit- See README
$connect_ticket = ''; 
$ipp_app_login  = '';
$ipp_app_id     = '';
$ipp_live       = false;


if(is_numeric($_GET['amount'])){
    
    
    $paypage = new HostedPayPage($connect_ticket, $ipp_app_login, $ipp_app_id, $ipp_live);
        
    $ticket = $paypage->get_ticket($_GET['amount']);
    
    if(is_array($ticket)  && isset($ticket['url'])){
        
        //If you are redirecting the customer back to your site 
        //Record into a dabase or other Verification system $ticket['OpId'] 
        //This will be used in the payment complete script to verify and match order details
        
        header('Location: '.$ticket['url']);
    }else{
        //Display Error Page
        echo "Sorry there was an error <br />";
        echo $pay->get_error();
    }
    
}else{
    
    //Display Error Page
    
    echo "Error -- Amount not a number";
}


