<html>
    <head>
    <title> Payment Successful -- Thank you</title>
    
    <style>
        
        body{
            background-color: #eee;
            margin: 0;
            
        }
        div {
            padding-left: 25px;
        }
        
        .header{
            height: 75px;
            line-height: 75px;
            background-color: steelblue;
            color: #FFF;
            padding-left: 15px;
            font-size: 2em;
                        
            
        }
        .divide{
            height: 25px;
            background-color: #333;
            color: #fff;
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
        }
        
        .success_lg{
            font-size: 2em;
            color: darkgreen;
            
              
        }
        
    </style>
    </head>
    <body>
        <div class="header"> My Company </div>
        <div class="divide">Home | About Us</div>
        <div>
            <div class="success_lg">Best Widget Ever</div>
            
            <p>You can now buy the Best Widget ever,  Only $99.99  </p>
            
            <p>If you would like to buy now,  <a href="example_redirect.php?amount=99.99">click here</a> to place your order on our secure payment processing page
            
            
        </div>
        
        
    </body>
</html>

        
       
    
    
    